FROM golang:1.10-stretch

WORKDIR /go/src/gitlab.com/trabel-b
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/trabel gitlab.com/trabel-b/app
