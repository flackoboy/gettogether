package trabel

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/gettogether/db/api"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/gettogether/config"
	"gitlab.com/gettogether/db"
	graceful "gopkg.in/tylerb/graceful.v1"
)

type App struct {
	log     *logan.Entry
	config  *config.Config
	db      *db.Repo
	ctx     context.Context
	cancel  func()
	web     Web
	healthy bool
}

func (a *App) AccountsQ() api.AccountQI {
	return api.NewAccountsQ(a.db)
}

func (a *App) CheckpointsQ() api.CheckpointsQI {
	return api.NewCheckpointsQ(a.db)
}

func (a *App) JourneysQ() api.JourneysQI {
	return api.NewJourneysQ(a.db)
}

func (a *App) CommentsQ() api.FeedbackQI {
	return api.NewFeedbackQ(a.db)
}

func NewApp(config *config.Config) *App {
	a := &App{
		config:  config,
		log:     config.Log(),
		healthy: false,
	}
	a.init()

	return a
}

func (a *App) init() {
	a.healthy = true
	appInit.Run(a)
}

func (a *App) Close() {
	a.cancel()
	a.db.DB.Close()
	a.healthy = false
}

func (a *App) Serve() {
	http.Handle("/", a.web.mux)

	addr := a.config.Listener().Addr().String()

	srv := graceful.Server{
		Timeout: 10 * time.Millisecond,

		Server: &http.Server{
			Addr:    addr,
			Handler: http.DefaultServeMux,
		},

		ShutdownInitiated: func() {
			a.log.Info("received signal, gracefully stopping")
			a.Close()
		},
	}

	a.log.Infof("starting trabel on %s", addr)

	err := srv.Serve(a.config.Listener())
	if err != nil {
		a.log.Panic(err)
	}
	a.log.Info("stopped")
}
