package main

import (
	"github.com/spf13/cast"
	"github.com/spf13/cobra"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	gettogether "gitlab.com/gettogether"
	"gitlab.com/gettogether/assets"
	"gitlab.com/gettogether/config"
)

type suppressablErr interface {
	SuppressStack() bool
}

var (
	// use exitCode to set process exit code instead of direct os.Exit call
	exitCode       int
	configInstance *config.Config
	rootCmd        = &cobra.Command{
		Use: "api",
	}
	runCmd = &cobra.Command{
		Use: "run",
		Run: func(cmd *cobra.Command, args []string) {
			defer func() {
				if rvr := recover(); rvr != nil {
					// TODO put suppresable errors into logan
					err := errors.FromPanic(rvr)
					entry := configInstance.Log()
					superr, ok := err.(suppressablErr)
					if !ok || !superr.SuppressStack() {
						entry = entry.WithStack(err)
					}
					entry.WithError(err).Error("app panicked")
				}
			}()
			applied, err := migrate("up", 0)
			log := configInstance.Log().WithField("applied", applied)
			if err != nil {
				log.WithError(err).Error("failed to apply migratations")
				return
			}
			log.Info("migrations applied")
			app := trabel.NewApp(configInstance)
			app.Serve()
		},
	}
	migrateCmd = &cobra.Command{
		Use:   "migrate [up|down|redo] [COUNT]",
		Short: "migrate schema",
		Long:  "performs a schema migration command",
		Run: func(cmd *cobra.Command, args []string) {
			log := configInstance.Log().WithField("service", "migration")
			var count int
			// Allow invocations with 1 or 2 args.  All other args counts are erroneous.
			if len(args) < 1 || len(args) > 2 {
				log.WithField("arguments", args).Error("wrong argument count")
				exitCode = 1
				return
			}
			// If a second arg is present, parse it to an int and use it as the count
			// argument to the migration call.
			if len(args) == 2 {
				var err error
				if count, err = cast.ToIntE(args[1]); err != nil {
					log.WithError(err).Error("failed to parse count")
					exitCode = 1
					return
				}
			}

			applied, err := migrate(args[0], count)
			log = log.WithField("applied", applied)
			if err != nil {
				log.WithError(err).Error("failed to apply migratations")
				return
			}
			log.Info("migrations applied")
		},
	}
)

func migrate(direction string, count int) (int, error) {
	applied, err := migrateDB(direction, count, configInstance.DB().DB.DB, assets.Migrations.Migrate)
	return applied, err
}

func main() {
	//defer os.Exit(exitCode)
	cobra.OnInitialize(func() {
		configInstance = config.New(kv.MustFromEnv())
	})
	rootCmd.AddCommand(runCmd)
	rootCmd.AddCommand(migrateCmd)
	rootCmd.Execute()
}
