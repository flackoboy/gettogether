package main

import (
	"database/sql"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/gettogether/assets"
	db2 "gitlab.com/trabel-b/db"
)

type Migrator func(*sql.DB, db2.MigrateDir, int) (int, error)

func migrateDB(direction string, count int, db *sql.DB, migrator Migrator) (int, error) {
	applied, err := migrator(db, db2.MigrateDir(direction), count)
	return applied, errors.Wrap(err, "failed to apply migrations")
}
