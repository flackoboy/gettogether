package assets

import "gitlab.com/distributed_lab/logan/v3"

//go:generate go-bindata -nometadata -ignore .+\.go$ -pkg assets -o bindata.go ./...
//go:generate gofmt -w bindata.go

const (
	migrationsDir = "migrations"
)

var (
	Migrations *MigrationsLoader
)

type AssetFn func(name string) ([]byte, error)
type AssetDirFn func(name string) ([]string, error)

func init() {
	Migrations = NewMigrationsLoader()
	if err := Migrations.loadDir(migrationsDir); err != nil {
		logan.New().WithField("service", "load-migrations").WithError(err).Fatal("failed to load migrations")
		return
	}
}
