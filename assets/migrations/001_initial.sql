-- +migrate Up

create table accounts (
    account_id bigserial primary key,
    nickname text not null,
    password_hash character varying(256) not null,
    phone character varying(256) not null,
    rules bigint not null,
    lat real not null,
    lng real not null
);

create unique index account_unique_constraint on accounts using btree(nickname, phone);

create table journeys (
    journey_id bigserial primary key,
    hash text not null,
    journey_type integer not null,
    title text not null,
    start_time timestamp not null,
    end_time timestamp,
    is_private boolean not null,
    creator_details jsonb DEFAULT '{}'::jsonb,
    in_danger boolean not null
);

create unique index journey_unique_constraint on journeys using btree(hash);

create table feedbacks (
    feedback_id bigserial primary key,
    comment text not null,
    stars integer not null,
    journey_id bigint not null,
    foreign key (journey_id) references journeys (journey_id)
);

create unique index feedback_unique_constraint on feedbacks using btree(feedback_id, journey_id);

create table checkpoints (
    checkpoint_id bigserial primary key,
    lat real not null,
    lng real not null,
    name text not null,
    difficulty integer not null
);

create unique index checkpoint_unique_constraint on checkpoints using btree(name, lat, lng);

create table account_active_journeys (
    account_id bigint references accounts(account_id) on update cascade on delete cascade,
    journey_id bigint references journeys(journey_id) on update cascade on delete cascade,
    journey_hash text not null,
    status integer not null,
    constraint account_active_journeys_pkey primary key (account_id, journey_id)
);

create unique index account_active_journeys_unique_constraint on account_active_journeys using btree(account_id, journey_id);

create table journey_checkpoints (
    journey_id bigint references journeys(journey_id) on update cascade on delete cascade,
    checkpoint_id bigint references checkpoints(checkpoint_id) on update cascade on delete cascade,
    seq_number bigint not null,
    constraint journey_checkpoints_pkey primary key (journey_id, checkpoint_id, seq_number)
);

create table medical_profiles (
    id bigserial primary key,
    account_id bigint references accounts(account_id) on update cascade on delete cascade,
    date_of_birth date not null,
    gender boolean not null,
    blood_type text not null,
    chronic_illnesses text
);

-- +migrate Down

drop table medical_profiles cascade ;
drop table journey_checkpoints cascade ;
drop table account_active_journeys cascade ;
drop table checkpoints cascade ;
drop table feedbacks cascade ;
drop table journeys cascade ;
drop table accounts cascade ;
