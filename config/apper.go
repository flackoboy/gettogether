package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type Apper interface {
	App() *App
}

type apper struct {
	once   comfig.Once
	getter kv.Getter
}

func NewApper(getter kv.Getter) Apper {
	return &apper{getter: getter}
}

func (a apper) App() *App {
	return a.once.Do(func() interface{} {
		var config struct {
			Passphrase  string `yaml:"passphrase"`
			Admin       string `yaml:"admin_seed"`
			DisableRBAC bool   `yaml:"disable_rbac"`
		}

		err := figure.Out(&config).From(kv.MustGetStringMap(a.getter, "app")).Please()
		if err != nil {
			panic(errors.Wrap(err, "cannot figure out app"))
		}

		//priv, err := ecdsa.GenerateKey(
		//	elliptic.P521(),
		//	bytes.NewReader([]byte(config.Admin)),
		//)
		//if err != nil {
		//	panic(err)
		//}
		//
		//pub, ok := priv.Public().(*ecdsa.PublicKey)
		//if !ok {
		//	panic("assertion error")
		//}
		//
		//h := sha1.New()
		//h.Write([]byte(pub.X.String() + pub.Y.String()))

		return &App{
			//AdminSignature: base64.URLEncoding.EncodeToString(h.Sum(nil)),
			DisableRBAC: config.DisableRBAC,
		}
	}).(*App)
}
