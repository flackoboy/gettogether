package config

import (
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/gettogether/db"
)

type DBer interface {
	DB() *db.Repo
}

type dber struct {
	getter kv.Getter
	once   comfig.Once
}

func NewDBer(getter kv.Getter) DBer {
	return &dber{getter: getter}
}

func (d *dber) DB() *db.Repo {
	return d.once.Do(func() interface{} {
		config := struct {
			URL     string `fig:"url,required"`
			MaxIdle int    `fig:"max_idle"`
			MaxOpen int    `fig:"max_open"`
		}{
			MaxIdle: 4,
			MaxOpen: 12,
		}

		if err := figure.Out(&config).From(kv.MustGetStringMap(d.getter, "db")).Please(); err != nil {
			panic(errors.Wrap(err, "failed to db"))
		}

		repo, err := db.Open(config.URL)
		if err != nil {
			panic(errors.Wrap(err, "failed to open database"))
		}

		repo.DB.SetMaxIdleConns(config.MaxIdle)
		repo.DB.SetMaxOpenConns(config.MaxOpen)

		if err := repo.DB.Ping(); err != nil {
			panic(errors.Wrap(err, "database is not accessible"))
		}

		return repo
	}).(*db.Repo)
}
