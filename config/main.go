package config

import (
	"gitlab.com/distributed_lab/kit/comfig"
	"gitlab.com/distributed_lab/kit/kv"
)

type (
	Config struct {
		DBer
		Apper
		comfig.Logger
		comfig.Listenerer
	}

	DB struct {
		DBName string `yaml:"db_name,required"`
		DBHost string `yaml:"db_host,required"`
		DBPort string `yaml:"db_port,required"`
		DBUser string `yaml:"db_user,required"`
		DBPass string `yaml:"db_pass,required"`
	}

	App struct {
		AdminSignature string `yaml:"admin_signature"`
		DisableRBAC    bool   `yaml:"disable_rbac"`
	}
)

func New(getter kv.Getter) *Config {
	return &Config{
		Listenerer: comfig.NewListenerer(getter),
		Logger:     comfig.NewLogger(getter, comfig.LoggerOpts{}),
		DBer:       NewDBer(getter),
		Apper:      NewApper(getter),
	}
}
