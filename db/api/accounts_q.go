package api

import (
	"database/sql"

	"github.com/fatih/structs"

	"gitlab.com/gettogether/internal/data"

	"gitlab.com/gettogether/db"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"

	sq "github.com/lann/squirrel"
)

type AccountQI interface {
	New() AccountQI
	Create(*data.Account) error
	SetRules(*data.Account, int64) error
	All() ([]data.Account, error)
	AccountExists(aID int64) (bool, *data.Account, error)
	NicknameOrPhoneRegistered(nickname, phone string) (bool, error)
	SetState(*data.Account) (int64, error)
	Get(nickname, passwordHash string) (*data.Account, error)

	GetMedicalInfo(aID int64) (*data.MedicalProfile, error)
	CreateMedicalInfo(i *data.MedicalProfile) (int64, error)
	UpdateMedicalInfo(i *data.MedicalProfile) (int64, error)
	HasActiveJourney(jID, aID, status int64) (bool, error)
}

type AccountsQ struct {
	repo     *db.Repo
	selector sq.SelectBuilder
	updater  sq.UpdateBuilder
	inserter sq.InsertBuilder
}

func (q AccountsQ) Get(nickname, passwordHash string) (*data.Account, error) {
	stmt := sq.
		Select("a.*").
		From("accounts a").
		Where("a.nickname = ? AND a.password_hash = ?", nickname, passwordHash)

	var res data.Account
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return &res, err
}

func (q AccountsQ) GetByNickname(nickname string) (*data.Account, error) {
	stmt := sq.
		Select("a.*").
		From("accounts a").
		Where("a.nickname = ?", nickname)

	var res data.Account
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return &res, err
}

func (q AccountsQ) NicknameOrPhoneRegistered(nickname, phone string) (bool, error) {
	stmt := sq.
		Select("1").
		From("accounts a").
		Where("a.nickname = ? OR a.phone = ?", nickname, phone)

	var res uint
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
	}
	return res == 1, err
}

func (q AccountsQ) AccountExists(aID int64) (bool, *data.Account, error) {
	stmt := q.selector.Where(sq.Eq{"a.account_id": aID})

	var res data.Account
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil, nil
		}
	}
	return res.AccountID == aID, &res, err
}

func NewAccountsQ(repo *db.Repo) *AccountsQ {
	return &AccountsQ{
		repo: repo,
		selector: sq.Select(
			"a.*",
			"account_id",
			"nickname",
			"password_hash",
			"phone",
			"rules",
			"lat",
			"lng",
		).From("accounts a"),
		updater:  sq.Update("accounts"),
		inserter: sq.Insert("accounts"),
	}
}

func (q *AccountsQ) New() AccountQI {
	return NewAccountsQ(q.repo)
}

func (q *AccountsQ) Create(a *data.Account) error {
	clauses := map[string]interface{}{
		"nickname":      a.Nickname,
		"password_hash": a.PasswordHash,
		"phone":         a.Phone,
		"rules":         a.Rules,
		"lat":           a.Lat,
		"lng":           a.Lng,
	}

	stmt := q.inserter.SetMap(clauses).Suffix(
		"on conflict (nickname, phone) do nothing returning account_id")

	return q.repo.Get(&(a.AccountID), stmt)
}

func (q *AccountsQ) SetRules(a *data.Account, newRules int64) error {
	stmt := q.updater.
		Set("rules", newRules).
		Where("account_id", a.AccountID).
		Suffix("returning account_id")

	err := q.repo.Get(&(a.AccountID), stmt)
	if err != nil {
		return errors.Wrap(err, "cannot update account rules", logan.F{
			"selector": stmt,
		})
	}
	return nil
}

func (q *AccountsQ) All() ([]data.Account, error) {
	var result []data.Account
	err := q.repo.Select(&result, q.selector)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}

	return result, err
}

func (q *AccountsQ) SetState(a *data.Account) (int64, error) {
	clauses := structs.Map(a)

	stmt := q.updater.SetMap(clauses).Where(sq.Eq{"account_id": a.AccountID})
	_, err := q.repo.Exec(stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
	}
	return a.AccountID, err
}

func (q *AccountsQ) CreateMedicalInfo(i *data.MedicalProfile) (int64, error) {
	clauses := structs.Map(i)

	stmt := sq.Insert("medical_profiles").Values(clauses).
		Suffix(
			"on conflict (account_id) do update " +
				"set date_of_birth = excluded.date_of_birth, " +
				"gender = excluded.gender, " +
				"blood_type = excluded.blood_type, " +
				"chronic_illnesses = excluded.chronic_illnesses",
		)
	_, err := q.repo.Exec(stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
	}

	return i.ID, err
}

func (q *AccountsQ) UpdateMedicalInfo(i *data.MedicalProfile) (int64, error) {
	clauses := structs.Map(i)

	stmt := sq.Update("medical_profiles").Where(sq.Eq{"account_id": i.AccountID}).SetMap(clauses)
	_, err := q.repo.Exec(stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return 1, nil
		}
	}

	return i.ID, err
}

func (q *AccountsQ) GetMedicalInfo(aID int64) (*data.MedicalProfile, error) {
	stmt := sq.
		Select("id", "account_id", "date_of_birth", "gender", "blood_type", "chronic_illnesses").
		From("medical_profiles").
		Where(sq.Eq{"account_id": aID})

	var res data.MedicalProfile
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return &res, err
}

func (q *AccountsQ) HasActiveJourney(jID, aID, status int64) (bool, error) {
	stmt := sq.
		Select("*").
		From("account_active_journeys").
		Where(sq.Eq{
			"account_id": aID,
			"journey_id": jID,
			"status":     status,
		})

	var res uint
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
	}
	return res == 1, err
}
