package api

import (
	"database/sql"

	sq "github.com/lann/squirrel"
	"gitlab.com/trabel-b/db"
	"gitlab.com/trabel-b/internal/data"
)

type CheckpointsQI interface {
	New() CheckpointsQI
	All() ([]data.Checkpoint, error)
	Create(checkpoint *data.Checkpoint) error
	ByJourneyID(jID uint) ([]data.Checkpoint, error)
	CheckpointExists(cID uint) (bool, error)
}

type CheckpointsQ struct {
	repo     *db.Repo
	selector sq.SelectBuilder
	inserter sq.InsertBuilder
	updater  sq.UpdateBuilder
}

func (q CheckpointsQ) CheckpointExists(cID uint) (bool, error) {
	stmt := sq.Select("1").From("checkpoints c").Where(sq.Eq{"c.checkpoint_id": cID})

	var res uint
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func NewCheckpointsQ(repo *db.Repo) CheckpointsQI {
	return &CheckpointsQ{
		repo: repo,
		selector: sq.Select(
			"lat",
			"lng",
			"name",
			"difficulty",
		).From("checkpoints c"),
		inserter: sq.Insert("checkpoints"),
		updater:  sq.Update("checkpoints"),
	}
}

func (q *CheckpointsQ) New() CheckpointsQI {
	return NewCheckpointsQ(q.repo)
}

func (q *CheckpointsQ) Create(c *data.Checkpoint) error {
	clauses := map[string]interface{}{
		"lat":        c.Lat,
		"lng":        c.Lng,
		"name":       c.Name,
		"difficulty": c.Difficulty,
	}
	stmt := q.inserter.SetMap(clauses).Suffix(
		"on conflict (name, lat, lng) do nothing returning checkpoint_id")
	return q.repo.Get(&(c.CheckpointID), stmt)
}

func (q *CheckpointsQ) All() ([]data.Checkpoint, error) {
	var result []data.Checkpoint
	err := q.repo.Select(&result, q.selector)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}

	return result, err
}

func (q *CheckpointsQ) ByJourneyID(jID uint) ([]data.Checkpoint, error) {
	stmt := q.selector.
		RightJoin("journey_checkpoints jc ON c.checkpoint_id = jc.checkpoint_id").
		Where(sq.Eq{"jc.journey_id": jID}).
		OrderBy("jc.seq_number")

	var res []data.Checkpoint
	err := q.repo.Select(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return res, err
}
