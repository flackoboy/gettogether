package api

import (
	"database/sql"

	sq "github.com/lann/squirrel"
	"gitlab.com/gettogether/db"
	"gitlab.com/gettogether/internal/data"
)

type FeedbackQI interface {
	New() FeedbackQI

	AddComment(f *data.Feedback) error
	DeleteComment(cID uint) (uint, error)
}

type FeedbackQ struct {
	repo     *db.Repo
	selector sq.SelectBuilder
	inserter sq.InsertBuilder
	updater  sq.UpdateBuilder
	deleter  sq.DeleteBuilder
}

func NewFeedbackQ(repo *db.Repo) FeedbackQI {
	return FeedbackQ{
		repo: repo,
		selector: sq.Select(
			"f.*",
			"feedback_id",
			"comment",
			"stars",
			"journey_id",
		).From("feedbacks f"),
		inserter: sq.Insert("feedbacks"),
		updater:  sq.Update("feedbacks"),
		deleter:  sq.Delete("feedbacks"),
	}
}

func (q FeedbackQ) New() FeedbackQI {
	return NewFeedbackQ(q.repo)
}

func (q FeedbackQ) AddComment(f *data.Feedback) error {
	clauses := map[string]interface{}{
		"comment":    f.Comment,
		"stars":      f.Stars,
		"journey_id": f.JourneyID,
	}

	stmt := q.inserter.SetMap(clauses).Suffix("returning feedback_id")

	return q.repo.Get(&(f.FeedbackID), stmt)
}

func (q FeedbackQ) DeleteComment(cID uint) (uint, error) {
	stmt := q.deleter.Where(sq.Eq{"feedback_id": cID}).Suffix("returning feedback_id")

	var deleted uint
	err := q.repo.Get(&deleted, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
	}
	return deleted, err
}
