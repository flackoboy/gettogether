package api

import (
	"database/sql"
	"fmt"

	"gitlab.com/distributed_lab/logan/v3/errors"

	sq "github.com/lann/squirrel"
	"gitlab.com/gettogether/db"
	"gitlab.com/gettogether/internal/data"
)

type JourneysQI interface {
	New() JourneysQI
	All() ([]data.Journey, error)
	Create(*data.Journey) error
	JourneyExists(jID uint) (bool, error)
	HasCheckpoint(jID, cID uint) (bool, uint, error)
	AddCheckpoint(jID, cID, seq uint) (uint, error)
	Checkpoints(jID uint) ([]data.Checkpoint, error)
	HasParticipant(jID, aID uint) (bool, error)
	AddParticipant(jID, aID, status uint) error
	Participants(jID uint) ([]data.Account, error)
	SetEmergency(jID uint, status bool) error
	SetState(j *data.Journey) (uint, error)
	ByID(jID uint) (*data.Journey, error)
}

type JourneysQ struct {
	repo     *db.Repo
	selector sq.SelectBuilder
	inserter sq.InsertBuilder
	updater  sq.UpdateBuilder
}

func (q JourneysQ) AddCheckpoint(jID, cID, seq uint) (uint, error) {
	stmt := sq.Insert("journey_checkpoints").Values(jID, cID, seq).Suffix("returning seq_number")

	var res uint
	err := q.repo.Get(&res, stmt)
	if err != nil {
		return 0, errors.Wrap(err, "cannot insert to journey_checkpoints")
	}
	return res, nil
}
func (q JourneysQ) AddParticipant(jID, aID, status uint) error {
	stmt := sq.
		Insert("account_active_journeys").
		Values(aID, jID, status).
		Suffix("on conflict (account_id, journey_id) do update set status = ?", status)

	_, err := q.repo.Exec(stmt)
	if err != nil {
		return errors.Wrap(err, "cannot insert to account_active_journeys")
	}
	return nil
}

func (q JourneysQ) JourneyExists(jID uint) (bool, error) {
	stmt := sq.
		Select("1").
		From("journeys j").
		Where(sq.Eq{"j.journey_id": jID})

	var res uint
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (q JourneysQ) HasCheckpoint(jID, cID uint) (bool, uint, error) {
	stmt := sq.
		Select("1").
		From("journey_checkpoints jc").
		Where(sq.Eq{"journey_id": jID, "checkpoint_id": cID}).
		Suffix("returning jc.seq_number")

	var seqNum uint
	err := q.repo.Get(&seqNum, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, 0, err
		}
	}
	return seqNum > 0, seqNum, err
}

func (q JourneysQ) HasParticipant(jID, aID uint) (bool, error) {
	stmt := sq.
		Select("1").
		From("account_active_journeys aj").
		Where(sq.Eq{"journey_id": jID, "account_id": aID})

	var res uint
	err := q.repo.Get(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
	}
	return res == 1, err
}

func NewJourneysQ(repo *db.Repo) JourneysQI {
	return JourneysQ{
		repo: repo,
		selector: sq.Select(
			"j.*",
			"journey_id",
			"hash",
			"journey_type",
			"title",
			"start_time",
			"end_time",
			"is_private",
			"creator_details",
			"in_danger",
		).From("journeys j"),
		inserter: sq.Insert("journeys"),
		updater:  sq.Update("journeys"),
	}
}

func (q JourneysQ) New() JourneysQI {
	return NewJourneysQ(q.repo)
}

func (q JourneysQ) All() ([]data.Journey, error) {
	result := make([]data.Journey, 0)
	err := q.repo.Select(&result, q.selector)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}

	for i := 0; i < len(result); i++ {
		result[i].Participants = []data.Account{}
		result[i].Checkpoints = []data.Checkpoint{}

		p, err := q.Participants(result[i].JourneyID)
		if err != nil {
			return result, err
		}
		result[i].Participants = append(result[i].Participants, p...)

		c, err := q.Checkpoints(result[i].JourneyID)
		if err != nil {
			return result, err
		}
		result[i].Checkpoints = append(result[i].Checkpoints, c...)
	}

	return result, err
}

func (q JourneysQ) Create(j *data.Journey) error {
	clauses := map[string]interface{}{
		"hash":            j.Hash,
		"journey_type":    j.Type,
		"title":           j.Title,
		"start_time":      j.StartTime,
		"end_time":        j.EndTime,
		"is_private":      j.IsPrivate,
		"creator_details": j.CreatorDetails,
		"in_danger":       j.InDanger,
	}
	stmt := q.inserter.SetMap(clauses).Suffix(
		"on conflict (hash) do nothing returning journey_id")
	return q.repo.Get(&(j.JourneyID), stmt)
}

func (q JourneysQ) SetEmergency(jID uint, status bool) error {
	stmt := q.updater.Where(sq.Eq{"journey_id": jID}).Set("in_danger", status)
	_, err := q.repo.Exec(stmt)
	if err != nil {
		return errors.Wrap(err, "cannot set in_danger")
	}
	return nil
}

func (q JourneysQ) SetState(j *data.Journey) (uint, error) {
	clauses := map[string]interface{}{
		"journey_id":      j.JourneyID,
		"hash":            j.Hash,
		"journey_type":    j.Type,
		"title":           j.Title,
		"start_time":      j.StartTime,
		"end_time":        j.EndTime,
		"is_private":      j.IsPrivate,
		"creator_details": j.CreatorDetails,
		"in_danger":       j.InDanger,
	}

	stmt := q.updater.SetMap(clauses).Where(sq.Eq{"journey_id": j.JourneyID})
	_, err := q.repo.Exec(stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return 0, nil
		}
	}
	return j.JourneyID, err
}

func (q JourneysQ) ByID(jID uint) (*data.Journey, error) {
	stmt := q.selector.Where(sq.Eq{"journey_id": jID})

	res := data.Journey{
		Participants: []data.Account{},
		Checkpoints:  []data.Checkpoint{},
	}
	err := q.repo.Get(&res, stmt)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			return nil, nil
		default:
			return nil, err
		}
	}

	journeyParticipants, err := q.Participants(jID)
	if err != nil {
		return nil, err
	}
	if journeyParticipants != nil {
		res.Participants = append(res.Participants, journeyParticipants...)
	}

	journeyCheckpoints, err := q.Checkpoints(jID)
	if err != nil {
		return nil, err
	}
	if journeyCheckpoints != nil {
		res.Checkpoints = append(res.Checkpoints, journeyCheckpoints...)
	}

	return &res, nil
}

func (q JourneysQ) Participants(jID uint) ([]data.Account, error) {
	accountIDsSelect, _, err := sq.
		Select("aj.account_id").
		From("account_active_journeys aj").
		Where(sq.Eq{"journey_id": jID}).ToSql()
	if err != nil {
		return nil, err
	}

	stmt := sq.Select(
		"a.*",
		"account_id",
		"nickname",
		"phone",
		"rules",
		"lat",
		"lng",
	).
		From("accounts a").
		Where(fmt.Sprintf("a.account_id IN (%s)", accountIDsSelect), jID) // magic with subquery

	var res []data.Account
	err = q.repo.Select(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return res, err
}

func (q JourneysQ) Checkpoints(jID uint) ([]data.Checkpoint, error) {
	checkpointIDsSelect, _, err := sq.
		Select("jc.checkpoint_id").
		From("journey_checkpoints jc").
		Where(sq.Eq{"journey_id": jID}).ToSql()

	if err != nil {
		return nil, err
	}

	stmt := sq.Select(
		"c.*",
		"checkpoint_id",
		"lat",
		"lng",
		"name",
		"difficulty",
	).
		From("checkpoints c").
		Where(fmt.Sprintf("c.checkpoint_id IN (%s)", checkpointIDsSelect), jID) // magic with subquery

	var res []data.Checkpoint
	err = q.repo.Select(&res, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
	}
	return res, err
}
