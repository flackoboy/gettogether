package trabel

import (
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/horizon/log"
)

type InitFn func(*App)

type initializer struct {
	Name string
	Fn   InitFn
	Deps []string
}

type initializerSet []initializer

var appInit initializerSet

func (is *initializerSet) Add(name string, fn InitFn, deps ...string) {
	*is = append(*is, initializer{
		Name: name,
		Fn:   fn,
		Deps: deps,
	})
}

func (is *initializerSet) Run(a *App) {
	err := is.checkDuplicates()
	if err != nil {
		a.log.WithError(err).Error("cannot init app with initializers")
		panic(err)
	}

	inits := *is
	alreadyRan := make(map[string]bool)

	for {
		ranSomething := false
		for _, initer := range inits {
			if _, ok := alreadyRan[initer.Name]; ok {
				continue
			}

			readyToRun := true
			for _, dep := range initer.Deps {
				if !alreadyRan[dep] {
					readyToRun = false
					break
				}
			}

			if !readyToRun {
				alreadyRan[initer.Name] = false
				continue
			}

			log.WithField("name", initer.Name).Debug("running initializer")
			initer.Fn(a)
			alreadyRan[initer.Name] = true
			ranSomething = true
		}
		if !ranSomething {
			break
		}

		if len(alreadyRan) != len(inits) {
			unrunned := make([]string, 0, len(inits)-len(alreadyRan))
			for _, i := range inits {
				if _, ok := alreadyRan[i.Name]; !ok {
					unrunned = append(unrunned, i.Name)
				}
			}
			a.log.WithFields(logan.F{"initializers": unrunned}).Error("Failed to run all initializers. It means that we have initializers cycle")
			panic(err)
		}
	}
}

func (is *initializerSet) checkDuplicates() error {
	init := *is

	unique := make(map[string]struct{}, len(*is))

	for _, initer := range init {
		if _, exists := unique[initer.Name]; exists {
			return errors.From(errors.New("duplicated initializer"), logan.F{
				"name": initer.Name,
			})
		}
	}
	return nil
}
