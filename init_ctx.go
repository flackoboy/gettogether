package trabel

import "context"

func initCtx(a *App) {
	a.ctx, a.cancel = context.WithCancel(context.Background())
}

func init() {
	appInit.Add("app-context", initCtx)
}
