package trabel

func initRepo(a *App) {
	a.db = a.config.DB()
}

func init() {
	appInit.Add("db.init", initRepo)
}
