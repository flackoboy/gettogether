package trabel

func initLog(a *App) {
	a.log = a.config.Log()
}

func init() {
	appInit.Add("log.init", initLog)
}
