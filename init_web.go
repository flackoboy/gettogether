package trabel

import (
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"github.com/go-chi/cors"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/web/handlers"
	mdw "gitlab.com/gettogether/web/middleware"
)

type Web struct {
	mux *chi.Mux
}

func initWeb(a *App) {
	mux := chi.NewMux()

	mux.Use(
		cors.New(cors.Options{
			AllowedOrigins:   []string{"*"},
			AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
			AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
			ExposedHeaders:   []string{"Link"},
			AllowCredentials: true,
			MaxAge:           300, // Maximum value not ignored by any of major browsers
		}).Handler,
	)

	a.web = Web{
		mux: mux,
	}
}

func initWebMiddlewares(a *App) {
	m := a.web.mux

	m.Use(
		middleware.StripSlashes,
		mdw.Ctx(
			ctx.CtxLog(a.log),
			ctx.CtxRepo(a.db),
			ctx.CtxIsAlive(a.healthy),
			ctx.CtxConfig(a.config),
			ctx.CtxAccountQ(a.AccountsQ()),
			ctx.CtxCheckpointsQ(a.CheckpointsQ()),
			ctx.CtxJourneysQ(a.JourneysQ()),
			ctx.CtxCommentsQ(a.CommentsQ()),
		),
		middleware.AllowContentType("application/json"),
	)

	m.NotFound(func(w http.ResponseWriter, r *http.Request) {
		err := problems.NotFound()
		err.Detail = "Unknown path"
		err.Meta = &map[string]interface{}{
			"url": r.URL.String(),
		}
		ape.RenderErr(w, err)
	})

	a.web.mux = m
}

func initWebHandlers(a *App) {
	m := a.web.mux

	m.Route("/api", func(m chi.Router) {

		m.Get("/isalive", handlers.GetIsAlive)

		m.Route("/users", func(r chi.Router) {
			r.Post("/login", handlers.Login)
			r.Post("/register", handlers.RegisterUser)
			r.Route("/{id}", func(r chi.Router) {
				r.Get("/", handlers.GetAccount)
				r.Post("/update_info", handlers.UpdateAccount)
				r.Get("/medical_info", handlers.GetMedicalInfo)
				r.Post("/medical_info", handlers.UpdateMedicalInfo)
				r.Put("/medical_info", handlers.CreateMedicalInfo)
				r.Post("/promote/{promotion-type}", handlers.ProvidePromotion)
				r.Post("/ban", handlers.BanUser)
				r.Post("/update_geo", handlers.UpdateUserGeo) // /update_geo?lat=x.xxxx&lng=y.yyyy
			})
		})

		m.Route("/journeys", func(r chi.Router) {
			r.Get("/", handlers.GetJourneysList)        // works
			r.Get("/{journey-id}", handlers.GetJourney) // works

			r.Post("/create", handlers.CreateJourney) // works
			r.Route("/{journey-id}", func(r chi.Router) {
				r.Post("/set_emergency/{status}", handlers.SetEmergency) // works

				r.Route("/checkpoints", func(r chi.Router) {
					r.Post("/add/{check-id}", handlers.AddCheckpoint) // works
					r.Get("/", handlers.GetJourneyCheckpoints)        // works
				})

				r.Route("/participants", func(r chi.Router) {
					r.Post("/add/{account-id}", handlers.AddParticipant)                  // todo idk works it or no
					r.Post("/access_request/{account-id}", handlers.RequestJourneyAccess) // todo idk how it works
					//r.Patch("/access_request/{account_id}", handlers.ReviewAccessRequest) // todo
					// todo add status to adding participant op:
					//  pending - participant was invited but not added
					// 	added - participant successfully registered (after his confirmation)
					//  declined - declined
				})

				r.Route("/comments", func(r chi.Router) {
					r.Post("/add", handlers.AddJourneyComment)
					r.Delete("/delete/{id}", handlers.RemoveJourneyComment)
					// todo editing comments
				})
			})

		})

		m.Route("/checkpoints", func(r chi.Router) {
			r.Post("/create", handlers.CreateCheckpoint)
		})

	})
}

func init() {
	appInit.Add("web.init", initWeb, "app-context")
	appInit.Add("web.middlewares", initWebMiddlewares, "web.init")
	appInit.Add("web.handlers", initWebHandlers, "web.middlewares", "web.init")
}
