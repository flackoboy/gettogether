package data

type Account struct {
	AccountID int64 `db:"account_id" structs:"account_id,omitempty" json:"account_id"`
	//Signature    string  `db:"signature" structs:"signature" json:"signature"`
	Nickname     string  `db:"nickname" structs:"nickname,omitempty" json:"nickname"`
	PasswordHash string  `db:"password_hash" structs:"password_hash,omitempty" json:"password_hash"`
	Phone        string  `db:"phone" structs:"phone,omitempty" json:"phone"`
	Rules        uint64  `db:"rules" structs:"rules,omitempty" json:"rules"`
	Lat          float64 `db:"lat" structs:"lat,omitempty" json:"lat"`
	Lng          float64 `db:"lng" structs:"lng,omitempty" json:"lng"`
}
