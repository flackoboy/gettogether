package data

type Checkpoint struct {
	CheckpointID uint    `structs:"checkpoint_id,omitempty" db:"checkpoint_id" json:"checkpoint_id"`
	Lat          float64 `structs:"lat,omitempty" db:"lat" json:"lat"`
	Lng          float64 `structs:"lng,omitempty" db:"lng" json:"lng"`
	Name         string  `structs:"name,omitempty" db:"name" json:"name"`
	Difficulty   int64   `structs:"difficulty,omitempty" db:"difficulty" json:"difficulty"`
}
