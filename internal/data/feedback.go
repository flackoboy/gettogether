package data

type Feedback struct {
	FeedbackID uint   `structs:"feedback_id,omitempty" db:"feedback_id" json:"feedback_id"`
	Comment    string `structs:"comment,omitempty" db:"comment" json:"comment"`
	Stars      int32  `structs:"stars,omitempty" db:"stars" json:"stars"`
	JourneyID  uint   `structs:"journey_id,omitempty" db:"journey_id" json:"journey_id"`
}
