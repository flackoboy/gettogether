package data

import "time"

type Journey struct {
	JourneyID      uint         `structs:"journey_id,omitempty" db:"journey_id" json:"journey_id"`
	Hash           string       `structs:"hash,omitempty" db:"hash" json:"hash"`
	Type           uint32       `structs:"journey_type,omitempty" db:"journey_type" json:"journey_type"`
	Title          string       `structs:"title,omitempty" db:"title" json:"title"`
	EndTime        *time.Time   `structs:"end_time,omitempty" db:"end_time" json:"end_time"`
	StartTime      time.Time    `structs:"start_time,omitempty" db:"start_time" json:"start_time"`
	IsPrivate      bool         `structs:"is_private,omitempty" db:"is_private" json:"is_private"`
	CreatorDetails string       `structs:"creator_details,omitempty" db:"creator_details" json:"creator_details"`
	InDanger       bool         `structs:"in_danger,omitempty" db:"in_danger" json:"in_danger"`
	Participants   []Account    `structs:"participants" json:"participants"`
	Checkpoints    []Checkpoint `structs:"checkpoints" json:"checkpoints"`
}
