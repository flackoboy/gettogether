package data

import "time"

type MedicalProfile struct {
	ID               int64     `db:"id" structs:"id,omitempty" json:"id"`
	AccountID        int64     `db:"account_id" structs:"account_id,omitempty" json:"account_id"`
	DateOfBirth      time.Time `db:"date_of_birth" structs:"date_of_birth,omitempty" json:"date_of_birth"`
	Gender           bool      `db:"gender" structs:"gender,omitempty" json:"gender"`
	BloodType        string    `db:"blood_type" structs:"blood_type,omitempty" json:"blood_type"`
	ChronicIllnesses string    `db:"chronic_illnesses" structs:"chronic_illnesses,omitempty" json:"chronic_illnesses"`
}
