package data

type RuleAction uint64

const (
	RuleActionBanAccount RuleAction = 1 << iota
	RuleActionPromoteAccount
	RuleActionCreateCheckpoints
	RuleActionAddParticipantToJourney
	RuleActionAddCheckpointToJourney
	RuleActionCreateGroupJourneys
	RuleActionEndGroupJourneys
	RuleActionCreateJourneys
	RuleActionViewJourneys
	RuleActionRemoveJourneys
	RuleActionCreateEmergencySignals
	RuleActionRemoveEmergencySignals
)
