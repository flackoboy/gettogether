package doormansvc

import (
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"github.com/spf13/cast"
	"gitlab.com/gettogether/internal/data"
)

func CheckRBAC(r *http.Request, constraints ...data.RuleAction) bool {
	if ctx.Config(r).App().DisableRBAC {
		return true
	}

	rawRules := r.Header.Get("rules")
	if rawRules == "" {
		return false
	}

	rules := cast.ToUint64(rawRules)
	allowed := true
	for _, rule := range constraints {
		allowed = allowed && (data.RuleAction(rules&uint64(rule)) == rule)
	}
	return allowed
}
