package resources

import (
	"github.com/spf13/cast"
	"gitlab.com/gettogether/internal/data"
)

type AccountParticipationStatus uint32

const (
	ParticipantStatusPending = iota + 1
	ParticipantStatusAccepted
	ParticipantStatusActive
	ParticipantStatusFinished
	ParticipantStatusDeclined
	ParticipantStatusRequestedSpectatorAccess
	ParticipantStatusSpectator
)

type Account struct {
	Key
	Attributes    AccountAttributes    `json:"attributes"`
	Relationships AccountRelationships `json:"relationships"`
}

type AccountAttributes struct {
	Nickname     string  `json:"nickname"`
	Phone        string  `json:"phone"`
	Rules        uint64  `json:"rules"`
	PasswordHash string  `json:"password_hash"`
	Lat          float64 `json:"lat"`
	Lng          float64 `json:"lng"`
}

type AccountRelationships struct {
	ActiveJourneys *RelationCollection `json:"active_journeys"`
}

type AccountResponse struct {
	Data *Account `json:"data"`
}

type AccountsResponse struct {
	Data []Account `json:"data"`
}

func NewAccount(account *data.Account) *Account { // todo we must set rules

	return &Account{
		Key: NewAccountKey(account.AccountID),
		Attributes: AccountAttributes{
			Nickname: account.Nickname,
			Phone:    account.Phone,
			Rules:    account.Rules,
			Lat:      account.Lat,
			Lng:      account.Lng,
		},
		Relationships: AccountRelationships{},
	}
}

func NewAccountKey(id int64) Key {
	return Key{
		ID:   cast.ToString(id),
		Type: Accounts,
	}
}
