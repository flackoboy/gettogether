package resources

import (
	"github.com/dewski/spatial"
	"github.com/spf13/cast"
	"gitlab.com/gettogether/internal/data"
)

type Checkpoint struct {
	Key
	Attributes CheckpointAttributes `json:"attributes,required"`
}

type CheckpointAttributes struct {
	Geo        spatial.Point `json:"geo"`
	Name       string        `json:"name"`
	Difficulty int64         `json:"difficulty"`
}

func NewCheckpointKey(id uint) Key {
	return Key{
		ID:   cast.ToString(id),
		Type: Checkpoints,
	}
}

func NewCheckpoint(c *data.Checkpoint) Checkpoint {
	return Checkpoint{
		Key: NewCheckpointKey(c.CheckpointID),
		Attributes: CheckpointAttributes{
			Geo: spatial.Point{
				Lat: c.Lat,
				Lng: c.Lng,
			},
			Name:       c.Name,
			Difficulty: c.Difficulty,
		},
	}
}
