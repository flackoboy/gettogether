package resources

type Feedback struct {
	Key
	Attributes    FeedbackAttributes    `json:"attributes"`
	Relationships FeedbackRelationships `json:"relationships"`
}

type FeedbackAttributes struct {
	Comment Details `json:"comment"`
	Stars   int32   `json:"stars"`
}

type FeedbackRelationships struct {
	Journey *Relation `json:"journey"`
}
