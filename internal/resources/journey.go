package resources

import (
	"time"

	"github.com/spf13/cast"

	"gitlab.com/gettogether/internal/data"
)

type JourneyType uint32

const (
	JourneyTypeIndividual JourneyType = 1
	JourneyTypeDuo
	JourneyTypeGroup
)

type Journey struct {
	Key
	Attributes    JourneyAttributes    `json:"attributes,required"`
	Relationships JourneyRelationships `json:"relationships,required"`
}

type JourneyAttributes struct {
	Type           JourneyType `json:"type,required"`
	Hash           string      `json:"hash"`
	Title          string      `json:"title,required"`
	EndTime        *time.Time  `json:"end_time"`
	StartTime      time.Time   `json:"start_time,required"`
	IsPrivate      bool        `json:"is_private"`
	CreatorDetails Details     `json:"creator_details"`
	InDanger       bool        `json:"in_danger"`
}

type JourneyRelationships struct {
	Participants *RelationCollection `json:"participants"`
	Checkpoints  *RelationCollection `json:"checkpoints"`
}

type JourneyResponse struct {
	Data Journey `json:"data"`
}

type JourneysResponse struct {
	Data []Journey `json:"data"`
}

func NewJourney(journey *data.Journey) *Journey {
	participantsC := RelationCollection{
		Data: []Key{},
	}
	for _, p := range journey.Participants {
		participantsC.Data = append(participantsC.Data, NewAccountKey(p.AccountID))
	}

	checkpointsC := RelationCollection{
		Data: []Key{},
	}
	for _, c := range journey.Checkpoints {
		checkpointsC.Data = append(checkpointsC.Data, NewCheckpointKey(c.CheckpointID))
	}

	return &Journey{
		Key: NewJourneyKey(journey.JourneyID),
		Attributes: JourneyAttributes{
			Type:           JourneyType(journey.Type),
			Hash:           journey.Hash,
			Title:          journey.Title,
			EndTime:        journey.EndTime,
			StartTime:      journey.StartTime,
			IsPrivate:      journey.IsPrivate,
			CreatorDetails: Details(journey.CreatorDetails),
			InDanger:       journey.InDanger,
		},
		Relationships: JourneyRelationships{
			Participants: &participantsC,
			Checkpoints:  &checkpointsC,
		},
	}
}

func NewJourneyKey(id uint) Key {
	return Key{
		ID:   cast.ToString(id),
		Type: Journeys,
	}
}
