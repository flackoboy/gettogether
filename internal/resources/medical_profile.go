package resources

import (
	"time"

	"gitlab.com/gettogether/internal/data"

	"github.com/spf13/cast"
)

type MedicalProfile struct {
	Key
	Attributes    MedicalProfileAttributes    `json:"attributes"`
	Relationships MedicalProfileRelationships `json:"relationships"`
}

type MedicalProfileAttributes struct {
	DateOfBirth      time.Time `json:"date_of_birth"`
	Gender           bool      `json:"gender"`
	BloodType        string    `json:"blood_type"`
	ChronicIllnesses string    `json:"chronic_illnesses"`
}

type MedicalProfileRelationships struct {
	Account *Relation `json:"account"`
}

type MedicalProfileResponse struct {
	Data *MedicalProfile `json:"data"`
}

func NewMedicalProfileKey(id int64) Key {
	return Key{
		ID:   cast.ToString(id),
		Type: MedicalProfiles,
	}
}

func NewMedicalProfile(profile *data.MedicalProfile) *MedicalProfile {
	return &MedicalProfile{
		Key: NewMedicalProfileKey(profile.ID),
		Attributes: MedicalProfileAttributes{
			DateOfBirth:      profile.DateOfBirth,
			Gender:           profile.Gender,
			BloodType:        profile.BloodType,
			ChronicIllnesses: profile.ChronicIllnesses,
		},
		Relationships: MedicalProfileRelationships{
			Account: NewAccountKey(profile.AccountID).AsRelation(),
		},
	}
}
