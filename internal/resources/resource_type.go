package resources

type ResourceType string

const (
	Accounts        ResourceType = "accounts"
	Journeys        ResourceType = "journeys"
	Feedbacks       ResourceType = "feedbacks"
	Checkpoints     ResourceType = "checkpoints"
	MedicalProfiles ResourceType = "medical-profiles"
)
