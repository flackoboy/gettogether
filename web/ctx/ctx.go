package ctx

import (
	"context"
	"net/http"

	"gitlab.com/gettogether/db/api"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/gettogether/config"
	"gitlab.com/gettogether/db"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	repoCtxKey
	isAliveKey
	configCtxKey
	accountCtxKey
	checkpointsCtxKey
	journeysCtxKey
	feedbackCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxRepo(repo *db.Repo) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, repoCtxKey, repo)
	}
}

func Repo(r *http.Request) *db.Repo {
	return r.Context().Value(repoCtxKey).(*db.Repo)
}

func CtxIsAlive(isAlive bool) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, isAliveKey, isAlive)
	}
}

func IsAlive(r *http.Request) bool {
	return r.Context().Value(isAliveKey).(bool)
}

func CtxConfig(config *config.Config) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, configCtxKey, config)
	}
}

func Config(r *http.Request) *config.Config {
	return r.Context().Value(configCtxKey).(*config.Config)
}

func CtxAccountQ(q api.AccountQI) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, accountCtxKey, q)
	}
}

func AccountsQ(r *http.Request) api.AccountQI {
	return r.Context().Value(accountCtxKey).(api.AccountQI).New()
}

func CtxCheckpointsQ(q api.CheckpointsQI) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, checkpointsCtxKey, q)
	}
}

func CheckpointsQ(r *http.Request) api.CheckpointsQI {
	return r.Context().Value(checkpointsCtxKey).(api.CheckpointsQI).New()
}

func CtxJourneysQ(q api.JourneysQI) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, journeysCtxKey, q)
	}
}

func JourneysQ(r *http.Request) api.JourneysQI {
	return r.Context().Value(journeysCtxKey).(api.JourneysQI).New()
}

func CtxCommentsQ(q api.FeedbackQI) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, feedbackCtxKey, q)
	}
}

func FeedbackQ(r *http.Request) api.FeedbackQI {
	return r.Context().Value(feedbackCtxKey).(api.FeedbackQI).New()
}
