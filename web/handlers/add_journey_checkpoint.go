package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/internal/doormansvc"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func AddCheckpoint(w http.ResponseWriter, r *http.Request) {
	if !doormansvc.CheckRBAC(r, data.RuleActionAddCheckpointToJourney) {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	journeyID, err := cast.ToUintE(chi.URLParam(r, "journey-id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse journey id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	existingJourney, err := ctx.JourneysQ(r).ByID(journeyID)
	switch {
	case err == nil:
		if existingJourney == nil {
			ape.RenderErr(w, problems.NotFound())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("journeyExists failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	checkpointID, err := cast.ToUintE(chi.URLParam(r, "check-id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse checkpoint id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	checkpointExists, err := ctx.CheckpointsQ(r).CheckpointExists(checkpointID)
	switch {
	case err == nil:
		if !checkpointExists {
			ape.RenderErr(w, problems.NotFound())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("checkpointExists failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	checkpointCount := len(existingJourney.Checkpoints)
	seqNumber, err := ctx.JourneysQ(r).AddCheckpoint(journeyID, checkpointID, cast.ToUint(checkpointCount+1))
	if err != nil {
		ctx.Log(r).
			WithError(err).
			WithField("journey_id", journeyID).
			WithField("checkpoint_id", checkpointID).
			Error("cannot add checkpoint to the journey")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	json.NewEncoder(w).Encode(struct {
		JourneyID    uint `json:"journey_id"`
		CheckpointID uint `json:"checkpoint_id"`
		SeqNumber    uint `json:"seq_number"`
	}{
		JourneyID:    journeyID,
		CheckpointID: checkpointID,
		SeqNumber:    seqNumber,
	})
}
