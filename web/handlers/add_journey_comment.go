package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/web/requests"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func AddJourneyComment(w http.ResponseWriter, r *http.Request) {
	journeyID, err := cast.ToUintE(chi.URLParam(r, "journey-id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse journey id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	journeyExists, err := ctx.JourneysQ(r).JourneyExists(journeyID)
	switch {
	case err == nil:
		if !journeyExists {
			ape.RenderErr(w, problems.NotFound())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("journeyExists failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	request, err := requests.NewAddJourneyCommentRequest(r)
	if err != nil {
		ctx.Log(r).WithError(err).Error("bad request body")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	dbFeedback := data.Feedback{
		Comment:   string(request.Data.Attributes.Comment),
		Stars:     request.Data.Attributes.Stars,
		JourneyID: journeyID,
	}

	if err = ctx.FeedbackQ(r).AddComment(&dbFeedback); err != nil {
		ctx.Log(r).WithError(err).Error("cannot add comment")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status   int           `json:"status"`
		Resource data.Feedback `json:"request"`
	}{
		Status:   http.StatusOK,
		Resource: dbFeedback,
	})
}
