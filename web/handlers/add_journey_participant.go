package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/resources"

	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/internal/doormansvc"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func AddParticipant(w http.ResponseWriter, r *http.Request) {
	if !doormansvc.CheckRBAC(r, data.RuleActionAddParticipantToJourney) {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	journeyID, err := cast.ToUintE(chi.URLParam(r, "journey-id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse journey id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	journeyExists, err := ctx.JourneysQ(r).JourneyExists(journeyID)
	switch {
	case err == nil:
		if !journeyExists {
			ape.RenderErr(w, problems.NotFound())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("journeyExists failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	accountID, err := cast.ToInt64E(chi.URLParam(r, "account-id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse participant id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	accountExists, _, err := ctx.AccountsQ(r).AccountExists(accountID)
	switch {
	case err == nil:
		if !accountExists {
			ape.RenderErr(w, problems.NotFound())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("accountExists failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	hasParticipant, err := ctx.JourneysQ(r).HasParticipant(journeyID, cast.ToUint(accountID))
	switch {
	case err == nil:
		if hasParticipant {
			ape.RenderErr(w, problems.Conflict())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("hasParticipant failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if err := ctx.JourneysQ(r).AddParticipant(journeyID, cast.ToUint(accountID), resources.ParticipantStatusPending); err != nil {
		ctx.Log(r).
			WithField("account_id", accountID).
			WithField("journey_id", journeyID).
			WithError(err).
			Error("cannot add participant to journey")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	jc := accountJourney{
		JourneyID: journeyID,
		AccountID: cast.ToUint(accountID),
	}

	json.NewEncoder(w).Encode(jc)
}

type accountJourney struct {
	JourneyID uint `db:"journey_id"`
	AccountID uint `db:"account_id"`
}
