package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/internal/doormansvc"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func BanUser(w http.ResponseWriter, r *http.Request) {
	if !doormansvc.CheckRBAC(r, data.RuleActionBanAccount) {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	accountID, err := cast.ToInt64E(chi.URLParam(r, "account-id"))
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	exists, account, err := ctx.AccountsQ(r).AccountExists(accountID)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot check whether account exists or no")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if !exists {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	account.Rules = 0

	updatedID, err := ctx.AccountsQ(r).SetState(account)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot update account state")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if updatedID != account.AccountID {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status uint `json:"status"`
	}{
		Status: http.StatusOK,
	})
}
