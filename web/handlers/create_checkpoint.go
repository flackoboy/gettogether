package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/doormansvc"

	"gitlab.com/gettogether/internal/data"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/web/requests"
)

func CreateCheckpoint(w http.ResponseWriter, r *http.Request) {
	if !doormansvc.CheckRBAC(r, data.RuleActionCreateCheckpoints) {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	request, err := requests.NewCreateCheckpointRequest(r)
	if err != nil {
		ctx.Log(r).WithError(err).Error("bad request body")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	// todo doorman
	dbCheckpoint := data.Checkpoint{
		Lng:        request.Data.Attributes.Geo.Lng,
		Lat:        request.Data.Attributes.Geo.Lat,
		Name:       request.Data.Attributes.Name,
		Difficulty: request.Data.Attributes.Difficulty,
	}

	if err := ctx.CheckpointsQ(r).Create(&dbCheckpoint); err != nil {
		ctx.Log(r).WithError(err).Error("cannot create checkpoint")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status   int             `json:"status"`
		Resource data.Checkpoint `json:"request"`
	}{
		Status:   http.StatusOK,
		Resource: dbCheckpoint,
	})
}
