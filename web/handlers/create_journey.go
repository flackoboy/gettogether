package handlers

import (
	"crypto/sha1"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/doormansvc"

	"gitlab.com/gettogether/internal/data"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/web/requests"
)

func CreateJourney(w http.ResponseWriter, r *http.Request) {
	if !doormansvc.CheckRBAC(r, data.RuleActionCreateJourneys) {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	request, err := requests.NewCreateJourneyRequest(r)
	if err != nil {
		ctx.Log(r).WithError(err).Error("bad request body")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	hs := sha1.New()
	raw := string(request.Data.Attributes.Type) + " " +
		request.Data.Attributes.Title + " " +
		cast.ToString(request.Data.Attributes.IsPrivate)

	hs.Write([]byte(raw))
	encodedHash := base64.URLEncoding.EncodeToString(hs.Sum(nil))

	if request.Data.Attributes.StartTime.IsZero() {
		request.Data.Attributes.StartTime = time.Now()
	}

	dbJourney := data.Journey{
		Hash:           encodedHash,
		Type:           uint32(request.Data.Attributes.Type),
		Title:          request.Data.Attributes.Title,
		StartTime:      request.Data.Attributes.StartTime,
		EndTime:        request.Data.Attributes.EndTime,
		IsPrivate:      request.Data.Attributes.IsPrivate,
		CreatorDetails: string(request.Data.Attributes.CreatorDetails),
	}

	if err := ctx.JourneysQ(r).Create(&dbJourney); err != nil {
		if err == sql.ErrNoRows {
			ctx.Log(r).WithError(err)
			ape.RenderErr(w, problems.Conflict())
			return
		}

		ctx.Log(r).WithError(err).Error("data saving error")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status   int          `json:"status"`
		Resource data.Journey `json:"request"`
	}{
		Status:   http.StatusOK,
		Resource: dbJourney,
	})
}
