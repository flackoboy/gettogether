package handlers

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/web/ctx"
	"gitlab.com/gettogether/web/requests"
)

func CreateMedicalInfo(w http.ResponseWriter, r *http.Request) {
	id := cast.ToInt64(chi.URLParam(r, "id"))

	request, err := requests.NewUpdateMedicalInfoRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
	}

	profileID, err := ctx.AccountsQ(r).CreateMedicalInfo(&data.MedicalProfile{
		AccountID:        id,
		DateOfBirth:      request.Data.Attributes.DateOfBirth,
		Gender:           request.Data.Attributes.Gender,
		BloodType:        request.Data.Attributes.BloodType,
		ChronicIllnesses: request.Data.Attributes.ChronicIllnesses,
	})

	if err != nil {
		ctx.Log(r).WithError(err).Error("failed to update medical info")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if profileID == -1 {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	ape.Render(w, http.StatusNoContent)
}
