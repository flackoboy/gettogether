package handlers

import (
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/internal/resources"
)

func GetAccount(w http.ResponseWriter, r *http.Request) {
	id := cast.ToInt64(chi.URLParam(r, "id"))

	_, user, err := ctx.AccountsQ(r).AccountExists(id)
	if err != nil {
		ctx.Log(r).WithError(err).Error("failed to get account")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if user == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	ape.Render(w, resources.AccountResponse{
		Data: resources.NewAccount(user),
	})
}
