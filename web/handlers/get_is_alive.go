package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/spf13/cast"
	"gitlab.com/gettogether/web/ctx"
)

type isAliveResponse struct {
	Data struct {
		Attributes struct {
			IsAlive string `json:"is_alive"`
		} `json:"attributes"`
	} `json:"data"`
}

func GetIsAlive(w http.ResponseWriter, r *http.Request) {
	isAlive := ctx.IsAlive(r)

	var response isAliveResponse

	response.Data.Attributes.IsAlive = cast.ToString(isAlive)

	json.NewEncoder(w).Encode(response)
}
