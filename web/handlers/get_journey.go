package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/web/requests"

	"gitlab.com/gettogether/internal/resources"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func GetJourney(w http.ResponseWriter, r *http.Request) {
	request := requests.NewGetJourneyRequest(r)

	// todo doorman

	journey, err := ctx.JourneysQ(r).ByID(cast.ToUint(request.Data.ID))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot get journey by id")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if journey == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	// todo private journeys (signature-based access)
	// do it with key got from /request_journey_access

	json.NewEncoder(w).Encode(resources.NewJourney(journey))
}
