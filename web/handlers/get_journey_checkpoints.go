package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/resources"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func GetJourneyCheckpoints(w http.ResponseWriter, r *http.Request) {
	journeyID, err := cast.ToUintE(chi.URLParam(r, "journey-id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse journey id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	checkpoints, err := ctx.CheckpointsQ(r).ByJourneyID(journeyID)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot get journey checkpoints by journey id")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if checkpoints == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	var response struct {
		Data []resources.Checkpoint `json:"data"`
	}

	for _, c := range checkpoints {
		response.Data = append(response.Data, resources.NewCheckpoint(&c))
	}

	json.NewEncoder(w).Encode(response)
}
