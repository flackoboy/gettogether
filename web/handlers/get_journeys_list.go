package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/internal/doormansvc"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/internal/resources"
)

func GetJourneysList(w http.ResponseWriter, r *http.Request) {
	if !doormansvc.CheckRBAC(r, data.RuleActionViewJourneys) {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	dbJourneys, err := ctx.JourneysQ(r).All() // todo pagination
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot get journeys")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if dbJourneys == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	response := resources.JourneysResponse{
		Data: []resources.Journey{},
	}

	for _, j := range dbJourneys {
		response.Data = append(response.Data, *resources.NewJourney(&j))
	}

	json.NewEncoder(w).Encode(response)
}
