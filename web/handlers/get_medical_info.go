package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/resources"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	"github.com/spf13/cast"

	"gitlab.com/gettogether/web/requests"
)

func GetMedicalInfo(w http.ResponseWriter, r *http.Request) {
	request := requests.NewGetMedicalInfoRequest(r)

	info, err := ctx.AccountsQ(r).GetMedicalInfo(cast.ToInt64(request.Data.ID))
	if err != nil {
		ctx.Log(r).WithError(err).Error("failed to get medical info")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if info == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	response := resources.MedicalProfileResponse{
		Data: resources.NewMedicalProfile(info),
	}

	json.NewEncoder(w).Encode(response)
}
