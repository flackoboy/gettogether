package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/resources"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"

	"gitlab.com/gettogether/web/requests"
)

func Login(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewLoginRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	user, err := ctx.AccountsQ(r).Get(request.Data.Attributes.Nickname, request.Data.Attributes.PasswordHash)
	if err != nil {
		ctx.Log(r).WithError(err).Error("failed to get account")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if user == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	json.NewEncoder(w).Encode(resources.AccountResponse{
		Data: resources.NewAccount(user),
	})
}
