package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/trabel-b/internal/doormansvc"

	"github.com/go-chi/chi"
	"github.com/gettogether/errors"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/internal/data"
)

func ProvidePromotion(w http.ResponseWriter, r *http.Request) {
	if !doormansvc.CheckRBAC(r, data.RuleActionPromoteAccount) {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	accountID, err := cast.ToInt64E(chi.URLParam(r, "account-id"))
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
	}

	exists, account, err := ctx.AccountsQ(r).AccountExists(accountID)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot check whether account exists or no")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if !exists {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	promotionType := chi.URLParam(r, "promotion-type")

	dbAccount := data.Account{
		Rules: account.Rules,
	}
	switch promotionType {
	case "subscribed_user":
		dbAccount.Rules = uint64(data.RuleAction(dbAccount.Rules) &
			data.RuleActionCreateJourneys &
			data.RuleActionAddParticipantToJourney &
			data.RuleActionRemoveEmergencySignals &
			data.RuleActionCreateGroupJourneys &
			data.RuleActionEndGroupJourneys &
			data.RuleActionRemoveJourneys &
			data.RuleActionCreateCheckpoints)
	case "emergency":
		dbAccount.Rules = uint64(data.RuleAction(dbAccount.Rules) &
			data.RuleActionRemoveJourneys &
			data.RuleActionRemoveEmergencySignals &
			data.RuleActionEndGroupJourneys)
	default:
		ape.RenderErr(w, problems.BadRequest(validation.Errors{
			"promotion-type": errors.New("invalid promotion type. Must be 'subscribed_user' or 'emergency'"),
		})...)
		return
	}

	updatedID, err := ctx.AccountsQ(r).SetState(&dbAccount)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot update account state")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if updatedID != account.AccountID {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status uint `json:"status"`
	}{
		Status: http.StatusOK,
	})
}
