package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/data"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/web/requests"
)

func RegisterUser(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewRegisterAccountRequest(r)
	if err != nil {
		ctx.Log(r).WithError(err).Error("bad request body")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	dbAccount := data.Account{
		Phone:        request.Data.Attributes.Phone,
		Rules:        uint64(data.RuleActionViewJourneys & data.RuleActionCreateEmergencySignals),
		Nickname:     request.Data.Attributes.Nickname,
		PasswordHash: request.Data.Attributes.PasswordHash,
		Lat:          request.Data.Attributes.Lat,
		Lng:          request.Data.Attributes.Lng,
	}

	alreadyRegistered, err := ctx.AccountsQ(r).NicknameOrPhoneRegistered(dbAccount.Nickname, dbAccount.Phone)
	switch {
	case err == nil:
		if alreadyRegistered {
			ape.RenderErr(w, problems.Conflict())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("NicknameOrPhoneRegistered failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if err = ctx.AccountsQ(r).Create(&dbAccount); err != nil {
		ctx.Log(r).WithError(err).Error("data saving error")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status   int          `json:"status"`
		Resource data.Account `json:"request"`
	}{
		Status:   http.StatusOK,
		Resource: dbAccount,
	})
}
