package handlers

import (
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/resources"

	"github.com/go-chi/chi"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/web/requests"
)

func RequestJourneyAccess(w http.ResponseWriter, r *http.Request) {
	request := requests.NewGetJourneyRequest(r)

	journey, err := ctx.JourneysQ(r).ByID(cast.ToUint(request.Data.ID))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot get journey by id")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if journey == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	accountID := chi.URLParam(r, "account-id")

	err = ctx.JourneysQ(r).
		AddParticipant(journey.JourneyID, cast.ToUint(accountID), resources.ParticipantStatusRequestedSpectatorAccess)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot update participant status")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
