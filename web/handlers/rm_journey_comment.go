package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func RemoveJourneyComment(w http.ResponseWriter, r *http.Request) {
	id, err := cast.ToUintE(chi.URLParam(r, "id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse comment id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	deleted, err := ctx.FeedbackQ(r).DeleteComment(id)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot delete comment")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if id != deleted {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status    uint `json:"status"`
		DeletedID uint `json:"deleted_id"`
	}{
		Status:    http.StatusOK,
		DeletedID: deleted,
	})
}
