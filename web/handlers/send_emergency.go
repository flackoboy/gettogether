package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/trabel-b/web/ctx"

	"github.com/go-errors/errors"
	validation "github.com/go-ozzo/ozzo-validation"

	"gitlab.com/gettogether/internal/resources"

	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/internal/doormansvc"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func SetEmergency(w http.ResponseWriter, r *http.Request) {
	status, err := cast.ToBoolE(chi.URLParam(r, "status"))
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	switch status {
	case true:
		if !doormansvc.CheckRBAC(r, data.RuleActionCreateEmergencySignals) {
			ape.RenderErr(w, problems.NotAllowed())
			return
		}
	case false:
		if !doormansvc.CheckRBAC(r, data.RuleActionRemoveEmergencySignals) {
			ape.RenderErr(w, problems.NotAllowed())
			return
		}
	}

	journeyID, err := cast.ToUintE(chi.URLParam(r, "journey-id"))
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	journeyExists, err := ctx.JourneysQ(r).JourneyExists(journeyID)
	switch {
	case err == nil:
		if !journeyExists {
			ape.RenderErr(w, problems.NotFound())
			return
		}
	default:
		ctx.Log(r).WithError(err).Error("journeyExists failed")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	body := struct {
		Requester int64 `json:"requester"`
	}{
		-1,
	}

	err = json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot decode body")
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if body.Requester == -1 {
		ape.RenderErr(w, problems.BadRequest(validation.Errors{
			"body/requester": errors.New("must be valid accountID"),
		})...)
	}

	isParticipant, err := ctx.AccountsQ(r).HasActiveJourney(int64(journeyID), body.Requester, resources.ParticipantStatusActive)
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot check journey participation")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if !isParticipant {
		ape.RenderErr(w, problems.NotAllowed())
		return
	}

	if err = ctx.JourneysQ(r).SetEmergency(journeyID, status); err != nil {
		ctx.Log(r).WithError(err).Error("cannot set emergency status")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	type emergencyJourney struct {
		JourneyID       uint `json:"journey_id"`
		EmergencyStatus bool `json:"emergency_status"`
	}

	json.NewEncoder(w).Encode(
		struct {
			Status   int
			Resource emergencyJourney
		}{
			Status: http.StatusOK,
			Resource: emergencyJourney{
				JourneyID:       journeyID,
				EmergencyStatus: status,
			},
		},
	)
}
