package handlers

import (
	"net/http"

	"github.com/spf13/cast"

	"gitlab.com/gettogether/web/ctx"

	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/gettogether/internal/data"
	"gitlab.com/gettogether/web/requests"
)

func UpdateAccount(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	request, err := requests.NewGetAccountRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	_, err = ctx.AccountsQ(r).SetState(&data.Account{
		AccountID:    cast.ToInt64(id),
		Nickname:     request.Data.Attributes.Nickname,
		PasswordHash: request.Data.Attributes.PasswordHash,
		Phone:        request.Data.Attributes.Phone,
		Rules:        request.Data.Attributes.Rules,
		Lat:          request.Data.Attributes.Lat,
		Lng:          request.Data.Attributes.Lng,
	})

	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot set account state")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	ape.Render(w, http.StatusNoContent)
}
