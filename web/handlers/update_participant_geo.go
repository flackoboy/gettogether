package handlers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gettogether/web/ctx"

	"gitlab.com/gettogether/internal/data"

	"github.com/go-chi/chi"

	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func UpdateUserGeo(w http.ResponseWriter, r *http.Request) {
	lat, err := cast.ToFloat64E(r.FormValue("lat"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("bad lat")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	lng, err := cast.ToFloat64E(r.FormValue("lng"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("bad lng")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	accountID, err := cast.ToInt64E(chi.URLParam(r, "id"))
	if err != nil {
		ctx.Log(r).WithError(err).Error("cannot parse account id")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	updatedState := data.Account{
		Lat:       lat,
		Lng:       lng,
		AccountID: accountID,
	}

	updatedID, err := ctx.AccountsQ(r).SetState(&updatedState)
	if err != nil {
		ctx.Log(r).
			WithField("account_id", accountID).
			WithError(err).
			Error("cannot update account state")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if updatedID != accountID {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	json.NewEncoder(w).Encode(struct {
		Status   int          `json:"status"`
		Resource data.Account `json:"request"`
	}{
		Status:   http.StatusOK,
		Resource: updatedState,
	})
}
