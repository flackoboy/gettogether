package requests

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/gettogether/internal/resources"
)

type AddJourneyCommentRequest struct {
	Data struct {
		Attributes resources.FeedbackAttributes `json:"attributes"`
	} `json:"data"`
}

func NewAddJourneyCommentRequest(r *http.Request) (*AddJourneyCommentRequest, error) {
	request := AddJourneyCommentRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "cannot unmarshal create_checkpoint_request")
	}
	return &request, request.Validate()
}

func (r *AddJourneyCommentRequest) Validate() error {
	data := &r.Data.Attributes
	//if err := validation.Validate(r.Data.Signature, validation.Required); err != nil {
	//	return err
	//}
	return validation.ValidateStruct(
		data,
		validation.Field(&data.Comment, validation.NilOrNotEmpty),
		validation.Field(&data.Stars, validation.Required, validation.Min(1), validation.Max(5)),
	)
}
