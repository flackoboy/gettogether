package requests

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/gettogether/internal/resources"
)

type CreateCheckpointRequest struct {
	Data struct {
		Attributes resources.CheckpointAttributes `json:"attributes"`
		Signature  string                         `json:"signature"`
	} `json:"data"`
}

func NewCreateCheckpointRequest(r *http.Request) (*CreateCheckpointRequest, error) {
	request := CreateCheckpointRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "cannot unmarshal create_checkpoint_request")
	}
	return &request, request.Validate()
}

func (r *CreateCheckpointRequest) Validate() error {
	data := &r.Data.Attributes
	//if err := validation.Validate(r.Data.Signature, validation.Required); err != nil {
	//	return err
	//}
	geo := &data.Geo

	if err := validation.ValidateStruct(
		geo,
		validation.Field(&geo.Lat, validation.Required, validation.Min(-90.0), validation.Max(90.0)),
		validation.Field(&geo.Lng, validation.Required, validation.Min(-180.0), validation.Max(180.0)),
	); err != nil {
		return err
	}

	return validation.ValidateStruct(
		data,
		validation.Field(&data.Name, validation.Required),
		validation.Field(&data.Difficulty, validation.Required, validation.Min(1), validation.Max(10)),
	)
}
