package requests

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/gettogether/internal/resources"
)

type CreateJourneyRequest struct {
	Data struct {
		Attributes resources.JourneyAttributes `json:"attributes"`
	} `json:"data"`
}

func NewCreateJourneyRequest(r *http.Request) (*CreateJourneyRequest, error) {
	request := CreateJourneyRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "cannot unmarshal create_journey_request")
	}
	return &request, request.Validate()
}

func (r *CreateJourneyRequest) Validate() error {
	data := &r.Data.Attributes

	return validation.ValidateStruct(
		data,
		validation.Field(&data.Type,
			validation.Required,
			validation.In(resources.JourneyTypeIndividual, resources.JourneyTypeDuo, resources.JourneyTypeGroup)),
		validation.Field(&data.Title, validation.Required),
		validation.Field(&data.CreatorDetails, validation.NilOrNotEmpty),
	)
}
