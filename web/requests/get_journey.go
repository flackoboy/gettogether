package requests

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"

	"gitlab.com/gettogether/internal/resources"
)

func NewGetJourneyRequest(r *http.Request) resources.JourneyResponse {
	return resources.JourneyResponse{
		Data: resources.Journey{
			Key: resources.NewJourneyKey(cast.ToUint(chi.URLParam(r, "journey-id"))),
		},
	}
}
