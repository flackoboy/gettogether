package requests

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/spf13/cast"
	"gitlab.com/gettogether/internal/resources"
)

func NewGetMedicalInfoRequest(r *http.Request) resources.AccountResponse {
	return resources.AccountResponse{
		Data: &resources.Account{
			Key: resources.NewAccountKey(cast.ToInt64(chi.URLParam(r, "id"))),
		},
	}
}
