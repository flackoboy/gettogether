package requests

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/gettogether/internal/resources"
)

type GetAccountRequest struct {
	Data struct {
		Attributes resources.AccountAttributes `json:"attributes"`
	} `json:"data"`
}

func NewLoginRequest(r *http.Request) (*GetAccountRequest, error) {
	request := GetAccountRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "failed to decode body")
	}
	return &request, request.Validate()
}

func (r *GetAccountRequest) Validate() error {
	data := &r.Data.Attributes
	return validation.ValidateStruct(
		data,
		validation.Field(&data.PasswordHash, validation.Required),
		validation.Field(&data.Nickname, validation.Required, is.Alphanumeric),
	)
}
