package requests

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/gettogether/internal/resources"
)

type RegisterAccountRequest struct {
	Data struct {
		Attributes resources.AccountAttributes `json:"attributes"`
	} `json:"data"`
}

func NewRegisterAccountRequest(r *http.Request) (*RegisterAccountRequest, error) {
	request := RegisterAccountRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "failed to decode body")
	}
	return &request, request.Validate()
}

func (rr *RegisterAccountRequest) Validate() error {
	data := &rr.Data.Attributes
	return validation.ValidateStruct(
		data,
		validation.Field(&data.Phone, validation.Required),
		validation.Field(&data.PasswordHash, validation.Required),
		validation.Field(&data.Nickname, validation.Required, is.Alphanumeric),
		validation.Field(&data.Lat, validation.Required, validation.Min(-90.0), validation.Max(90.0)),
		validation.Field(&data.Lng, validation.Required, validation.Min(-180.0), validation.Max(180.0)),
	)
}
