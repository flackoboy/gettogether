package requests

import (
	"encoding/json"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

func NewGetAccountRequest(r *http.Request) (*GetAccountRequest, error) {
	request := GetAccountRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "failed to decode body")
	}
	return &request, ValidateGetAccount(request)
}

func ValidateGetAccount(r GetAccountRequest) error {
	data := &r.Data.Attributes
	return validation.ValidateStruct(
		data,
		validation.Field(&data.Phone, validation.Required),
		validation.Field(&data.Lng, validation.Required, is.Longitude),
		validation.Field(&data.Lat, validation.Required, is.Latitude),
		validation.Field(&data.PasswordHash, validation.Required),
		validation.Field(&data.Nickname, validation.Required, is.Alphanumeric),
	)
}
