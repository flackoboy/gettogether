package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3/errors"

	"gitlab.com/gettogether/internal/resources"
)

type UpdateMedicalInfoRequest struct {
}

func NewUpdateMedicalInfoRequest(r *http.Request) (*resources.MedicalProfileResponse, error) {
	var request resources.MedicalProfileResponse

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, errors.Wrap(err, "failed to decode body")
	}

	return &request, nil
}
